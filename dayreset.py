"""
Path of Exile Daily Timer and Utils
(c)2017 Justyn Chaykowski
github.com/frawst

Contributors:
Justyn Chaykowski - Frawst
** - EclectickMedia

Functions
right_now(): Returns formatted string of server time if user in GMT-6

wait_time(): Returns formatted string of countdown to dailies reset

set_colors(): Returns color value based on length of countdown time

local_time(): Returns formatted string of local time GMT-6

pinger(): Handles pinging for network output

net_stat_log(str, str): Logs to file(str) the current network status(str)

cs_to_hms(int): Converts seconds to format 'H:M:S'

ping_timer(int, str): Handles the up-time timer, outputs seconds of uptime

log_exit(str): Just adds a line to log for exiting program

BRANCH 'ping-etc' CHANGELOG:
- PEP-8'd
- Added proper docstrings
- Added network status tracker
- Fixed a bug where the countdown timer showed incorrect values
- Added cs_to_hms() function
- Modified display to include GOOD-STATE timer
- Updated timer to act independent from pinger()
- Improved logging to only print GOOD status once per start / recovery
- Added exit-program to logging
- Encased main program loop and functionality into main()
- Began building test suite for timer
    - test_tz()
- Added __name__ check
- Fixed the timer issues where CD could exceed 24 hours, and always
  showed time until reset as an hour too long.
- Added variables so program can handle any TZ, now just need to
  implement some system to get TZ of current system
- Fixed an issue where timer minutes were showing a minute too long

Developer Notes:
- Time until reset countdown will show XX:60:XX (NEEDS FIX)
    - Looks like this bug is prevalent for all minutes, that it shows
    one minute longer than it should. Seconds are okay, IDK about minutes.

- Daily countdown is fucked:
    - Broken when within one hour of reset
    - Displays exorbitant hours when reset was within 4 hours ago
    - Other issues may remain.. Time functions in general are kinda fucked
"""

import time
import pygame
import sys
import subprocess

"""Overall Project To-Do's
    Specific To-Do's to be listed in their relevant locations"""
# TODO: Add a Memory Usage meter
# TODO: Add a -game-uptime- readout (Time this launch)
# TODO: Add a 'Time-Played-Today' Readout
# TODO: Add buttons to launch other tools (PoB, TradeMacro, Mercury, etc.)

# Initialize pygame and font
pygame.init()
pygame.font.init()

# PYGAME GLOBALS
# TODO: Set display size to scale with elements instead of fixed size
# TODO: Display settings, font-size, font-family, etc?
SIZE = width, height = 300, 150  # Screen Dimensions
BLACK = 0, 0, 0
WHITE = 225, 225, 225
RED = 225, 0, 0
ORANGE = 245, 122, 45
YELLOW = 245, 245, 89
GREEN = 87, 245, 81
FONT = pygame.font.SysFont('impact', 22)

# TIME ZONE INFORMATION
TZ = ((-60)*60)*4  # GMT -4
RESET_HOUR = 20
LOCAL_TZ = ((-60)*60)*6  # GMT -6

# Other Variables
VERSION = '0.1'
YEAR = '2017'
HOLDER = 'Justyn Chaykowski'
CONTRIBUTORS = 'EclectickMedia'
VERSION_STRING = """Path of Exile Daily Timer and Utils v%s
(c) %s %s
Contributors: %s""" % (VERSION, YEAR, HOLDER, CONTRIBUTORS)

# PyGame screen set-up, set title
screen = pygame.display.set_mode(SIZE)
pygame.display.set_caption('PoE Daily Timer and Utils')

def test_tz(tz, ltz, reset):
    global TZ
    global LOCAL_TZ
    global RESET_HOUR

    TZ = (60*60) * tz
    LOCAL_TZ = (60*60) * ltz
    RESET_HOUR = reset


# TODO: Add handling for any and all timezones
def right_now():
    """
    Current Path of Exile SERVER TIME (H/M/S)
    :return: String
    """
    work = time.gmtime(time.time() + TZ)
    return time.strftime('%X', work)


# TODO: Hours seems to be an hour long? When under 1 hour wait, shows 1 hr.
# TODO: Also shows timer > 40Hours for 4 hours after reset
def wait_time():
    """
    Get time until reset (H/M/S)
    :return: String
    """
    global RESET_HOUR
    work = time.gmtime(time.time() + TZ)  # Get GMT-4 time
    minsec = 60  # Used to invert mins/secs for countdown
    reset_hour = RESET_HOUR  # The hour in GMT-4 that server resets on
    hour = (reset_hour - 1) - work[3]  # Set hour to fit countdown
    if hour < 0:
        #hour = 20 + work[3]  # Fix countdown if reset is over 19 hours away
        hour = 24 + hour

    # Format time pieces, and string
    if hour < 10:
        hour = '0%i' % hour
    minute = minsec - work[4] - 1
    if minute < 10:
        minute = '0%i' % minute
    second = minsec - work[5]
    if second < 10:
        second = '0%i' % second
    out_string = '%s:%s:%s' % (str(hour), str(minute), str(second))
    return out_string


def set_colors():
    """
    Outputs a color based on time until reset, color will be one of the
    globally defined colors.
    :return: VAR
    """
    global RESET_HOUR
    work = time.gmtime(time.time() + TZ)
    minsec = 60
    reset_hour = RESET_HOUR
    hour = reset_hour - work[3]
    if hour < 0:
        hour = 24 - work[3]
    minute = minsec - work[4] - 1
    second = minsec - work[5]
    wait_seconds = (hour*60*60) + (minute*60) + second
    day = 24*60*60
    quarter = day/4
    if wait_seconds > quarter*2:  # If wait exceeds 12 hours
        return RED
    elif wait_seconds > quarter:  # If wait exceeds 6 hours
        return ORANGE
    elif wait_seconds > (60*60):  # If wait exceeds 1 hour
        return YELLOW
    elif wait_seconds < (60*60):  # If wait is under 1 hour
        return GREEN
    else:  # Redundancy
        return RED


# Output a string of current local time (GMT -6)
def local_time():
    """
    Current local time H/M/S
    :return: String
    """
    work = time.gmtime(time.time() + LOCAL_TZ)
    state = 'AM'
    hour = work[3]
    if hour > 12:
        hour -= 12
        state = 'PM'
    minute = work[4]
    if minute < 10:
        minute = '0%i' % minute
    second = work[5]
    if second < 10:
        second = '0%i' % second
    return '%s:%s:%s %s' % (hour, minute, second, state)


# TODO: Add handling for various ping results
# TODO: Add a sound for when status returns to GREEN

# Critical pinger() variables
retry = False  # Whether or not a network failure has occurred recently
rest_count = 0  # Number of times network shows fine after a failure occurs
state_timer = time.time()  # Keep track of how long GREEN status is up


def pinger():
    """
    Handles the network status tracker

    When called will ping google.ca and check for the callback, if received
    then network condition is considered good. Otherwise, condition considered
    bad and will require 4 successful pings before outputting RESTORED, and
    a 5th successful ping before outputting GOOD. This function also handles
    it's own coloring outputted as one of the global colors.

    :return: String, Var
    """
    global retry
    global rest_count
    global state_timer
    hostname = 'google.ca'
    response, result = subprocess.getstatusoutput('ping -n 1 -w 1000 %s'
                                                  % hostname)

    if response == 0 and not retry:
        status = 'GOOD'
        color = GREEN
    elif response == 0 and retry and rest_count > 3:
        status = 'RESTORED'
        rest_count = 0
        retry = False
        color = YELLOW
    elif response == 0 and retry:
        status = 'Restoring...'
        rest_count += 1
        color = ORANGE
    else:
        re_check = 0
        print('%s: A failure may have occurred, running re-check.'
              % time.strftime('%X'))
        while re_check < 3:  # On failure, retry 3 times before declaring a failure
            time.sleep(0.3)
            response, result = subprocess.getstatusoutput('ping -n 1 -w 1000 %s'
                                                          % hostname)
            if response != 0:
                re_check += 1
                print('%s: double-checking failure state %i'
                      % (time.strftime('%X'), re_check))
            else:  # If failure recovers, return to GREEN
                print('%s: Potential failure has recovered, go green.'
                      % time.strftime('%X'))
                status = 'GOOD'
                color = GREEN
                return status, color
        print('%s: Potential failure confirmed, Go red.'
              % time.strftime('%X'))
        status = 'BAD'
        retry = True
        color = RED
        state_timer = time.time()

    return status, color


# Take time + state from pinger() to output int of network uptime
def ping_timer(green_when, state):
    if state == 'GOOD':
        return time.time() - green_when
    else:
        return 0

# TODO: Output more relevant info
'''include length of
up-time in readouts. Eventually use logging to include:
- RAM usage
- Playtime
- etc.'''

g_report = False


def net_stat_log(file, status):
    """
    Handles network status logging

    Output will append to the file continuously, new file-runs are marked
    by the full Date-Time the program was launched. Cleans up after itself.

    :param file: String
    :param status: String
    :return: None
    """
    global g_report
    if status != 'GOOD':
        g_report = False
    net_log = open(file, 'a')
    now = time.strftime('%X')
    if not g_report:
        log_string = '%s: %s\n' % (now, status)
        net_log.write(log_string)
    net_log.close()
    if status == 'GOOD':
        g_report = True


def log_exit(file):
    net_log = open(file, 'a')
    net_log.write('%s: Program Closed.' % time.strftime('%X'))
    net_log.close()


def cs_to_hms(seconds):
    """
    Convert seconds into format H:M:S
    :param seconds: Amount of seconds to convert
    :return: String 'H:M:S'
    """
    if seconds > 60:
        minutes = seconds / 60
        sec_remain = seconds % 60
        seconds = sec_remain

        if minutes > 60:
            hours = minutes / 60
            min_remain = minutes % 60
            minutes = min_remain
        else:
            hours = 0
    else:
        hours = 00
        minutes = 00

    if hours < 10:
        hours = '0%i' % int(hours)
    else:
        hours = int(hours)
    if minutes < 10:
        minutes = '0%i' % int(minutes)
    else:
        minutes = int(minutes)
    if seconds < 10:
        seconds = '0%i' % int(seconds)
    else:
        seconds = int(seconds)
    hours = str(hours)
    minutes = str(minutes)
    seconds = str(seconds)

    out = '%s:%s:%s' % (hours, minutes, seconds)
    return out

# Gather initial net status, start the new log section, loop prep
def main():
    ping_status, ping_color = pinger()
    loop_count = 0  # Number of times loop has repeated
    log = open('log.log', 'a')
    log.write('\nNetwork status logs for: %s\n' % time.asctime())
    log.close()
    print(VERSION_STRING)
    while True:
        # If the X button is clicked, close the program.
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                log_exit('log.log')
                pygame.display.quit()  # Redundant, should run on sys.exit()
                sys.exit()

        if loop_count % 5 == 0:  # Check and log network status every 5 loops
            ping_status, ping_color = pinger()
            net_stat_log('log.log', ping_status)

        state_timer_print = ping_timer(state_timer, ping_status)

        # PREP SHIT TO BE RENDERED
        screen.fill(BLACK)

        show_pinger_timer = FONT.render('Stable for: %s'
                                        % cs_to_hms(state_timer_print), 0, GREEN)
        show_pinger = FONT.render('Network Status: %s' % ping_status, 0, ping_color)
        show_local_time = FONT.render('Local Time: %s' % local_time(), 0, WHITE)
        show_time = FONT.render('Current Time (GMT -4): %s' % right_now(), 0, WHITE)
        t_color = set_colors()
        show_time_until_reset = FONT.render('Time Until Reset: %s'
                                            % wait_time(), 0, t_color)

        # ACTUALLY RENDER THE SHIT
        screen.blit(show_time, (5, 5))
        screen.blit(show_local_time, (5, 30))
        screen.blit(show_time_until_reset, (5, 55))
        screen.blit(show_pinger, (5, 80))
        screen.blit(show_pinger_timer, (5, 105))

        pygame.display.update()  # May be redundant.
        loop_count += 1  # Iterate loop counter
        time.sleep(1)  # Slow the script down, this doesn't need to run hyper-fast


if __name__ == "__main__":
    #test_tz(-4, -6, 20)
    main()
